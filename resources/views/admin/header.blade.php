<div class="p-3 mb-2 bg-secondary text-white">
<div class="container-fluid">
    <div class="d-flex justify-content-center">
        <div class="d-flex p-2">
            <a href="/admin/news-list?pageNumber=0&pageLimit=10" class="list-group-item list-group-item-action active">Hírek</a>
            <a href="#" class="list-group-item d-flex justify-content-between align-items-center">Rendezvények</a>
            <a href="/admin/document-list?pageNumber=0&pageLimit=10" class="list-group-item list-group-item-action active">Dokumentumok</a>
            <a href="/admin/info-list?pageNumber=0&pageLimit=10" class="list-group-item d-flex justify-content-between align-items-center">Információ</a>
            <a href="/admin/gallery-list?pageNumber=0&pageLimit=10" class="list-group-item list-group-item-action active">Galéria</a>
            <a href="/admin/logout" class="list-group-item d-flex justify-content-between align-items-center">Kijelentkezés</a>
            </div>
        </div>
</div>
</div>
