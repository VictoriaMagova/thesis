@extends('admin.layout')

@if($gallery)
    @section('title', '"' . $gallery->id . '" galéria szerkesztése')
@else
    @section('title', 'Új galéria létrehozása')
@endif

@section('content')
    <div class="p-2 mb-2 bg-secondary text-white">
    <div class="row">
        <div id="src" class="col-md-9">
            @if($gallery)
                <form method="post" class="form-group row" action="/admin/save-gallery">
                    @csrf
                    <input type="hidden" class="form-control" name="id" value="{{$gallery->id}}">
                    <label>
                        <input placeholder="Galéria neve" type="text" name="name" value="{{$gallery->name}}">
                    </label>

                    <button type="submit" class="btn btn-primary">Mentés</button>
                </form>
            @else
                <form method="post" class="form-group row" action="/admin/save-gallery">
                    @csrf
                    <label>
                        <input placeholder="Galéria neve" class="form-control mb-2" type="text" name="name">
                    </label>

                    <button type="submit" class="btn btn-primary">Mentés</button>
                </form>
            @endif
        </div>
    </div>
@stop
