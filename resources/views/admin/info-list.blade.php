@extends('admin.layout')
@section('title', 'Információ listája')
@section('content')
    <div class="p-2 mb-2 bg-secondary text-white">
    <div id="src" class="container-fluid">
        <table id="pager_add_news" class="table-center">
            <tr>
                <td>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            @if($pageNumber > 1)
                                <li class="page-item" >
                                    <a class="page-link" href="/admin/info-list?pageNumber={{$pageNumber-1}}&pageLimit={{$pageLimit}}" aria-label="Previous">
                                        <span aria-hidden="true" >&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                            @endif

                            @for($i = 0; $i < $pageTotal; $i++)
                                <li class="page-item">
                                    <a class="page-link @if($pageNumber === $i+1) active-pager @endif" href="/admin/info-list?pageNumber={{$i+1}}&pageLimit={{$pageLimit}}">{{$i + 1}}</a>
                                </li>
                            @endfor

                            @if($pageNumber < $pageTotal)
                                <li class="page-item">
                                    <a class="page-link" href="/admin/info-list?pageNumber={{$pageNumber+1}}&pageLimit={{$pageLimit}}" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </nav>
                </td>
                <td style="text-align: right">
                    <!--form method="get" action="news-detail.component" class="shadow p-3 mb-5 bg-white rounded"-->
                    <a href="/admin/info">
                        <button class="fa fa-plus btn btn-primary btn-sm"> Új információ</button>
                    </a>
                    <!--/form-->
                </td>
            </tr>
        </table>

        <table id="news_box" class="table table-striped table-dark">
            <tr>
                <td><span class="badge badge-primary">ID</span></td>
                <td><span class="badge badge-primary">Cím</span></td>
                <td><span class="badge badge-primary">Szerkesztés/Törlés</span></td>
                <td></td>
            </tr>
            @foreach($infoList as $info)
                <tr>
                    <td>{{$info->id}}</td>
                    <td>{{$info->info_name}}</td>
                    <td>
                        <a href="/admin/info?id={{$info->id}}" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                        <a href="/admin/delete-info?id={{$info->id}}" class="btn btn-default"><i class="fa fa-trash"></i></a>
                    </td>
                    <td>
                        <a href="/admin/{{$info->id}}/information-list">Tovább >></a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    </div>
@stop
