@extends('admin.layout')

@if($info)
    @section('title', '"' . $info->id . '" infó szerkesztése')
@else
    @section('title', 'Új információ létrehozása')
@endif

@section('content')
    <div class="p-2 mb-2 bg-secondary text-white">
    <div class="row">
        <div id="src" class="col-md-9">
            @if($info)
                <form method="post" class="form-group row" action="/admin/save-info">
                    @csrf
                    <input type="hidden" class="form-control" name="id" value="{{$info->id}}">
                    <label>
                        <input placeholder="Információ címe" type="text" name="name" value="{{$info->title}}">
                    </label>
                    <button type="submit" class="btn btn-primary">Mentés</button>
                </form>

            @else
                <form method="post" class="form-group row" action="/admin/save-info">
                    @csrf
                    <label>
                        <input placeholder="Információ címe" class="form-control mb-2" type="text" name="name">
                    </label>
                </form>
                <button type="submit" class="btn btn-primary">Mentés</button>
            @endif
        </div>
    </div>
@stop
