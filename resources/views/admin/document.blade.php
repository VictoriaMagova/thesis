@extends('admin.layout')

@if($document)
    @section('title', '"' . $document->id . '" dokumentum szerkesztése')
@else
    @section('title', 'Új dokumentum létrehozása')
@endif

@section('content')
    <div class="p-2 mb-2 bg-secondary text-white">
        <div class="row">
            <div id="src" class="col-md-9">
                <form method="post" action="/admin/save-document" enctype="multipart/form-data">
                    @csrf

                    <label>
                        <input placeholder="Dokumentum neve" type="text" class="form-control mb-2" name="name">
                    </label>
                    <br>
                    <label for="Product Name">Dokumentum feltöltése:</label>
                    <br/>
                    <input type="file" accept=".pdf" class="form-control" name="document"/>
                    <br/><br/>
                    <input type="submit" class="btn btn-primary" value="Feltöltés"/>
                </form>
            </div>
        </div>
@stop

