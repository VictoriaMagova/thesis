@extends('admin.layout')
@section('title', 'Képek listája')
@section('content')
    <div class="p-2 mb-2 bg-secondary text-white">
    <div id="src" class="container-fluid">
        <table id="pager_add_news" class="table-center">
            <tr>
                <td>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            @if($pageNumber > 1)
                                <li class="page-item" >
                                    <a class="page-link" href="/admin/{{$galleryId}}/image-list?pageNumber={{$pageNumber-1}}&pageLimit={{$pageLimit}}" aria-label="Previous">
                                        <span aria-hidden="true" >&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                            @endif

                            @for($i = 0; $i < $pageTotal; $i++)
                                <li class="page-item">
                                    <a class="page-link @if($pageNumber === $i+1) active-pager @endif" href="/admin/{{$galleryId}}/image-list?pageNumber={{$i+1}}&pageLimit={{$pageLimit}}">{{$i + 1}}</a>
                                </li>
                            @endfor

                            @if($pageNumber < $pageTotal)
                                <li class="page-item">
                                    <a class="page-link" href="/admin/{{$galleryId}}/image-list?pageNumber={{$pageNumber+1}}&pageLimit={{$pageLimit}}" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </nav>
                </td>
                <td style="text-align: right">
                    <!--form method="get" action="news-detail.component" class="shadow p-3 mb-5 bg-white rounded"-->
                    <a href="/admin/{{$galleryId}}/image">
                        <button class="fa fa-plus btn btn-primary btn-sm"> Új kép</button>
                    </a>
                    <!--/form-->
                </td>
            </tr>
        </table>

        <table id="news_box" class="table table-striped table-dark">
            <tr>
                <td><span class="badge badge-primary">ID</span></td>
                <td><span class="badge badge-primary">Kép</span></td>
                <td><span class="badge badge-primary">Szerkesztés/Törlés</span></td>
            </tr>
            @foreach($imageList as $image)
                <tr>
                    <td>{{$image->id}}</td>
                    <td><img src="{{$image->image_url}}" style="max-width: 100px"></td>
                    <td>
                        <a href="/admin/delete-image?id={{$image->id}}" class="btn btn-default"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    </div>
@stop
