@extends('admin.layout')
@section('title', 'Hírek listája')
@section('content')
<div class="p-2 mb-2 bg-secondary text-white">
    <div id="src" class="container-fluid">
        <table id="pager_add_news" class="table-center">
            <tr>
                <td>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            @if($pageNumber > 1)
                                <li class="page-item" >
                                    <a class="page-link" href="/admin/news-list?pageNumber={{$pageNumber-1}}&pageLimit={{$pageLimit}}" aria-label="Previous">
                                        <span aria-hidden="true" >&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                            @endif

                            @for($i = 0; $i < $pageTotal; $i++)
                                <li class="page-item">
                                    <a class="page-link @if($pageNumber === $i+1) active-pager @endif" href="/admin/news-list?pageNumber={{$i+1}}&pageLimit={{$pageLimit}}">{{$i + 1}}</a>
                                </li>
                            @endfor

                            @if($pageNumber < $pageTotal)
                                <li class="page-item">
                                    <a class="page-link" href="/admin/news-list?pageNumber={{$pageNumber+1}}&pageLimit={{$pageLimit}}" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </nav>
                </td>
                <td style="text-align: right">
                    <!--form method="get" action="news-detail.component" class="shadow p-3 mb-5 bg-white rounded"-->
                    <a href="/admin/news">
                        <button class="fa fa-plus btn btn-primary btn-sm"> Új hír</button>
                    </a>
                    <!--/form-->
                </td>
            </tr>
        </table>

        <table id="news_box" class="table table-striped table-dark">
            <tr>
                <td><span class="badge badge-primary">ID</span></td>
                <td><span class="badge badge-primary">Cím</span></td>
                <td><span class="badge badge-primary">Létrehozás</span></td>
                <td><span class="badge badge-primary">Szerkesztés/Törlés</span></td>
            </tr>
            @foreach($newsList as $news)
                <tr>
                    <td>{{$news->id}}</td>
                    <td>{{$news->title}}</td>
                    <td>{{$news->created_at}}</td>
                    <td>
                        <a href="/admin/news?id={{$news->id}}" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                        <a href="/admin/delete-news?id={{$news->id}}" class="btn btn-default"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@stop
