@extends('admin.layout')
@section('title', 'Galéria listája')
@section('content')
    <div class="p-2 mb-2 bg-secondary text-white">
    <div id="src" class="container-fluid" class="table-center">
        <table id="pager_add_news">
            <tr>
                <td>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            @if($pageNumber > 1)
                                <li class="page-item" >
                                    <a class="page-link" href="/admin/gallery-list?pageNumber={{$pageNumber-1}}&pageLimit={{$pageLimit}}" aria-label="Previous">
                                        <span aria-hidden="true" >&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                            @endif

                            @for($i = 0; $i < $pageTotal; $i++)
                                <li class="page-item">
                                    <a class="page-link @if($pageNumber === $i+1) active-pager @endif" href="/admin/gallery-list?pageNumber={{$i+1}}&pageLimit={{$pageLimit}}">{{$i + 1}}</a>
                                </li>
                            @endfor

                            @if($pageNumber < $pageTotal)
                                <li class="page-item">
                                    <a class="page-link" href="/admin/gallery-list?pageNumber={{$pageNumber+1}}&pageLimit={{$pageLimit}}" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </nav>
                </td>
                <td style="text-align: right">
                    <a href="/admin/gallery">
                        <button class="fa fa-plus btn btn-primary btn-sm"> Új galéria</button>
                    </a>
                </td>
            </tr>
        </table>

        <table id="news_box" class="table table-striped table-dark">
            <tr>
                <td><span class="badge badge-primary">ID</span></td>
                <td><span class="badge badge-primary">Név</span></td>
                <td><span class="badge badge-primary">Szerkesztés/Törlés</span></td>
                <td></td>
            </tr>
            @foreach($galleryList as $gallery)
                <tr>
                    <td>{{$gallery->id}}</td>
                    <td>{{$gallery->gallery_name}}</td>
                    <td>
                        <a href="/admin/gallery?id={{$gallery->id}}" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                        <a href="/admin/delete-gallery?id={{$gallery->id}}" class="btn btn-default"><i class="fa fa-trash"></i></a>
                    </td>
                    <td>
                        <a href="/admin/{{$gallery->id}}/image-list">Tovább >></a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@stop
