@extends('admin.layout')

@if($news)
    @section('title', '"' . $news->id . '" hír szerkesztése')
@else
    @section('title', 'Új hír létrehozása')
@endif

@section('content')
    <div class="p-2 mb-2 bg-secondary text-white">
        <div class="row">
        <div id="src" class="col-md-9">
            @if($news)
                <form method="post" class="form-group row" action="/admin/save-news">
                    @csrf
                    <input type="hidden" class="form-control" name="id" value="{{$news->id}}">
                    <label>
                        <input placeholder="Hír címe" type="text" class="form-control mb-2" name="title" value="{{$news->title}}">
                    </label>

                    <label>
                        <textarea placeholder="Hír leírása" class="form-control" name="newsContent">{{$news->news_content}}</textarea>
                    </label>

                    <button type="submit" class="btn btn-primary">Mentés</button>
                </form>

            @else

                <form method="post" class="form-group row" action="/admin/save-news">
                    @csrf
                    <label>
                        <input placeholder=" Hír címe" class="form-control mb-2" type="text" name="title">
                    </label>

                    <label>
                        <textarea placeholder="Hír leírása" class="form-control" name="newsContent"></textarea>
                    </label>

                    <button type="submit" class="btn btn-primary">Mentés</button>
                </form>
            @endif
        </div>
    </div>
@stop
