@extends('admin.layout')

@if($image)
    @section('title', '"' . $gallery->id . '" képek szerkesztése')
@else
    @section('title', 'Új kép hozzáadása')
@endif

@section('content')
    <div class="p-2 mb-2 bg-secondary text-white">
        <div class="row">
            <div id="src" class="col-md-9">
                <form method="post" action="/admin/save-image" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="galleryId" value="{{$galleryId}}">

                    <label for="Product Name">Product photos (can attach more than one):</label>
                    <br/>
                    <input type="file" accept=".png, .jpg, .jpeg" class="form-control" name="photos[]" multiple/>
                    <br/><br/>
                    <input type="submit" class="btn btn-primary" value="Upload"/>
                </form>
            </div>
        </div>
    </div>
@stop

