@extends('admin.layout')

@if($information)
    @section('title', '"' . $information->id . '" információ szerkesztése')
@else
    @section('title', 'Új információ létrehozása')
@endif

@section('content')
    <div class="p-2 mb-2 bg-secondary text-white">
    <div class="row">
        <div id="src" class="col-md-9">
            @if($information)
                <form method="post" class="form-group row" action="/admin/save-information">
                    @csrf
                    <input type="hidden" class="form-control" name="id" value="{{$information->id}}">
                    <label>
                        <input placeholder="Információ címe" type="text" name="informationName" value="{{$information->information_name}}">
                    </label>

                    <label>
                        <textarea placeholder="Információ szövege" class="form-control" name="informationContent">{{$information->information_content}}</textarea>
                    </label>
                    <button type="submit">Mentés</button>
                </form>
            @else
                <form method="post" class="form-group row" action="/admin/save-information">
                    @csrf
                    <input type="hidden" name="infoId" value="{{$infoId}}">

                    <label>
                        <input placeholder=" Információ címe" class="form-control mb-2" type="text" name="informationName">
                    </label>

                    <label>
                        <textarea placeholder="Információ szövege" class="form-control" name="informationContent"></textarea>
                    </label>

                    <button type="submit" class="btn btn-primary">Mentés</button>
                </form>
            @endif
        </div>
    </div>
@stop
