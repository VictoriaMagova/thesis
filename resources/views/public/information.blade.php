@extends('public.layout')
@section('title', $information->information_name . ' szaktanács')
@section('content')

    <div class="container info">
        <h2>{{$information->information_name}} szaktanács</h2>
        <p>
            {{$information->information_content}}
        </p>

    </div>

@stop