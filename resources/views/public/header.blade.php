<header>
    <div class="navbar">
        <a>
            <img src="/storage/image/logo.png" width="70%" height="50px">
        </a>
        <a href="/hirek">Hírek</a>
        <a href="/rendezvenyek">Rendezvények</a>
        <a href="/dokumentumon">Dokumentumok</a>
        <div class="subnav">
            <button class="subnavbtn">Információ <i class="fa fa-caret-down"></i></button>
            <div class="subnav-content">
                @if($infos)
                    @foreach($infos as $info)
                        <a href="/info?id={{$info->id}}" >{{$info->info_name}}</a>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="subnav">
            <button class="subnavbtn">Rólunk <i class="fa fa-caret-down"></i></button>
            <div class="subnav-content">
                <a href="#info1" onclick="Galeria()">Galéria</a>
                <a href="#info2" onclick="Valetabizottsag()">Valéta Bizottság</a>
            </div>
        </div>
        <a href="#alapitvany" onclick="Alapitvany()">Alapítvány</a>
    </div>
</header>

<section>
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="/storage/image/kep6_2.jpg" height=350px" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="/storage/image/kep7_2.jpg" height="350px" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="/storage/image/kep8_3.jpg" height="350px" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
