@extends('public.layout')
@section('title', 'Hírek')
@section('content')

    <div class="container">
        @if($newsList)
            @foreach($newsList as $news)
                <article>
                    <h2>{{$news->title}}</h2>
                    <div>
                        <p>
                            {{$news->news_content}}
                        </p>
                    </div>
                </article>
            @endforeach
        @endif
    </div>

@stop