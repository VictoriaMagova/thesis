@extends('public.layout')
@section('title', $info->info_name)
@section('content')

    <div class="container">
        <ul>
            @foreach($informationList as $information)
                <li class="info">
                    <a href="/informacio?id={{$information->id}}">
                        {{$information->information_name}}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>

@stop