<?php

namespace App\Http\Controllers;

use App\Models\Info;
use App\Models\Information;
use Illuminate\Http\Request;

class InfoController extends Controller
{
    public function showInfoList(Request $request)
    {
        $request = $request->all();

        $info = Info::find($request['id']);

        $informationList = Information::where('info_id', '=', $request['id'])
            ->get();

        return view('public/info-list', [
            'info' => $info,
            'informationList' => $informationList
        ]);
    }
}
