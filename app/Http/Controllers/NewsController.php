<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function showNewsList(Request $request)
    {
        $news = News::orderBy('id', 'desc')
            ->limit(5)
            ->get();

        return view('public/news-list', [
            'newsList' => $news
        ]);
    }

}
