<?php

namespace App\Http\Controllers;

use App\Models\Information;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    public function showInformation(Request $request)
    {
        $request = $request->all();

        $information = Information::find($request['id']);

        return view('public/information', [
            'information' => $information
        ]);
    }
}
