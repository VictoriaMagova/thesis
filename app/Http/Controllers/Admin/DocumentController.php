<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\DocumentTrait;
use App\Http\Controllers\Controller;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    use DocumentTrait;

    public function showDocumentList(Request $request)
    {
        $request = $request->all();

        $totalItemsCount = Document::count();

        if (!isset($request['pageLimit']) || !$request['pageNumber']) {
            return redirect('/admin/document-list?pageNumber=1&pageLimit=10');
        }

        $pageLimit = intval($request['pageLimit']);
        $pageNumber = intval($request['pageNumber']);

        $documentList = Document::orderBy('id', 'desc')
            ->limit($pageLimit)
            ->offset($pageLimit * ($pageNumber - 1))
            ->get();

        return view('admin/document-list', [
            'documentList' => $documentList,
            'pageNumber' => $pageNumber,
            'pageLimit' => $pageLimit,
            'pageTotal' => ceil($totalItemsCount / $pageLimit)
        ]);
    }

    public function showDocument(Request $request)
    {
        $request = $request->all();

        $document = null;
        if (isset($request['id'])) {
            $document = Document::find($request['id']);
        }

        return view('admin/document', ['document' => $document]);
    }

    public function saveDocument(Request $request)
    {
        $request = $request->all();

        /** @var Document $document */
        $document = new Document();
        $document->document_guid = uniqid();
        $document->document_name = $request['name'];
        $document->save();

        /** @var UploadedFile $uploadedDocument */
        $uploadedDocument = $request['document'];

        $savedDocumentUrl = Storage::disk('public')->putFileAs(
            'document',
            $uploadedDocument,
            getTransformedString($request['name'] . '-' .$document->id). '.pdf');

        $document->document_url = '/storage/'.$savedDocumentUrl;
        $document->save();

        return redirect('/admin/document-list?pageNumber=1&pageLimit=10');
    }

    public function deleteDocument(Request $request)
    {
        $request = $request->all();

        $document = Document::find($request['id']);
        $document->delete();

        return redirect()->back();
    }
}
