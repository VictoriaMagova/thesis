<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function showNewsList(Request $request)
    {
        $request = $request->all();

        $totalItemsCount = News::count();

        if (!isset($request['pageLimit']) || !$request['pageNumber']) {
            return redirect('/admin/news-list?pageNumber=1&pageLimit=10');
        }

        $pageLimit = intval($request['pageLimit']);
        $pageNumber = intval($request['pageNumber']);

        $newsList = News::orderBy('id', 'desc')
            ->limit($pageLimit)
            ->offset($pageLimit * ($pageNumber - 1))
            ->get();

        return view('admin/news-list', [
            'newsList' => $newsList,
            'pageNumber' => $pageNumber,
            'pageLimit' => $pageLimit,
            'pageTotal' => ceil($totalItemsCount / $pageLimit)
        ]);
    }

    public function showNews(Request $request)
    {
        $request = $request->all();

        $news = null;
        if (isset($request['id'])) {
            $news = News::find($request['id']);
        }

        return view('admin/news', ['news' => $news]);
    }

    public function saveNews(Request $request)
    {
        $request = $request->all();

        $news = null;

        if (isset($request['id'])) {
            /** @var News $news */
            $news = News::find($request['id']);
        } else {
            /** @var News $news */
            $news = new News();
            $news->news_guid = uniqid();
            $news->created_at = (new \DateTimeImmutable())->format('Y-m-d H:i:s');
        }

        $news->title = $request['title'];
        $news->news_content = $request['newsContent'];

        $news->save();

        return redirect('/admin/news-list?pageNumber=1&pageLimit=10');
    }

    public function deleteNews(Request $request)
    {
        $request = $request->all();

        $news = News::find($request['id']);
        $news->delete();

        return redirect()->back();
    }
}
