<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function showImageList($galleryId, Request $request)
    {
        $request = $request->all();

        $totalItemsCount = Image::count();

        if (!isset($request['pageLimit']) || !$request['pageNumber']) {
            return redirect('/admin/' .$galleryId. '/image-list?pageNumber=1&pageLimit=10');
        }

        $pageLimit = intval($request['pageLimit']);
        $pageNumber = intval($request['pageNumber']);

        $imageList = Image::where('gallery_id', '=', $galleryId)
            ->orderBy('id', 'desc')
            ->limit($pageLimit)
            ->offset($pageLimit * ($pageNumber - 1))
            ->get();

        return view('admin/image-list', [
            'imageList' => $imageList,
            'galleryId' => $galleryId,
            'pageNumber' => $pageNumber,
            'pageLimit' => $pageLimit,
            'pageTotal' => ceil($totalItemsCount / $pageLimit)
        ]);

    }

    public function showImage($galleryId, Request $request)
    {
        $request = $request->all();

        $image = null;
        if (isset($request['id'])) {
            $image = Image::find($request['id']);
        }

        return view('admin/image', [
            'galleryId' => $galleryId,
            'image' => $image
        ]);
    }

    public function saveImage(Request $request)
    {
        $request = $request->all();

        if(!isset($request['photos']) || empty($request['photos'])) {
            abort(404);
        }

        foreach ($request['photos'] as $photo) {
            /** @var UploadedFile $uploadedImage */
            $uploadedImage = $photo;

            /** @var Image $image */
            $image = new Image();
            $image->image_guid = uniqid();
            $image->gallery_id = $request['galleryId'];
            $image->save();

            $savedImageUrl = Storage::disk('public')->putFileAs(
                'image',
                $uploadedImage,
                'gallery-' .$request['galleryId']. '-image-' .$image->id. '.jpg');

            $image->image_url = '/storage/'.$savedImageUrl;
            $image->save();
        }

        return redirect('/admin/' .$request['galleryId']. '/image-list?pageNumber=1&pageLimit=10');
    }

    public function deleteImage(Request $request)
    {
        $request = $request->all();

        $image = Image::find($request['id']);
        $image->delete();
        return redirect()->back();
    }
}
