<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Info;
use Illuminate\Http\Request;

class InfoController extends Controller
{
    public function showInfoList(Request $request)
    {
        $request = $request->all();

        $totalItemsCount = Info::count();

        if (!isset($request['pageLimit']) || !$request['pageNumber']) {
            return redirect('/admin/info-list?pageNumber=1&pageLimit=10');
        }

        $pageLimit = intval($request['pageLimit']);
        $pageNumber = intval($request['pageNumber']);

        $infoList = Info::orderBy('id', 'desc')
            ->limit($pageLimit)
            ->offset($pageLimit * ($pageNumber - 1))
            ->get();

        return view('admin/info-list', [
            'infoList' => $infoList,
            'pageNumber' => $pageNumber,
            'pageLimit' => $pageLimit,
            'pageTotal' => ceil($totalItemsCount / $pageLimit)
        ]);
    }

    public function showInfo(Request $request)
    {
        $request = $request->all();

        $info = null;
        if (isset($request['id'])) {
            $info = Info::find($request['id']);
        }
        return view('admin/info' , ['info' => $info]);
    }

    public function saveInfo(Request $request)
    {
        $request = $request->all();

        $info = null;
        if (isset($request['id'])) {
            /** @var Info $info */
            $info = Info::find($request['id']);
        }else {
            /** @var Info $info */
            $info = new Info();
            $info->info_guid = uniqid();
        }
        $info->info_name = $request['name'];

        $info->save();

        return redirect('/admin/info-list?pageNumber=1&pageLimit=10');
    }

    public function deleteInfo(Request $request)
    {
        $request = $request->all();

        $info = Info::find($request['id']);
        $info->delete();
        return redirect()->back();
    }
}
