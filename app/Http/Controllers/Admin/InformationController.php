<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Information;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    public function showInformationList($infoId, Request $request)
    {
        $request = $request->all();

        $totalItemsCount = Information::count();

        if (!isset($request['pageLimit']) || !$request['pageNumber']) {
            return redirect('/admin/' .$infoId. '/information-list?pageNumber=1&pageLimit=10');
        }

        $pageLimit = intval($request['pageLimit']);
        $pageNumber = intval($request['pageNumber']);

        $informationList = Information::where('info_id', '=', $infoId)
            ->orderBy('id', 'desc')
            ->limit($pageLimit)
            ->offset($pageLimit * ($pageNumber - 1))
            ->get();

        return view('admin/information-list', [
            'informationList' => $informationList,
            'infoId' => $infoId,
            'pageNumber' => $pageNumber,
            'pageLimit' => $pageLimit,
            'pageTotal' => ceil($totalItemsCount / $pageLimit)
        ]);
    }

    public function showInformation($infoId, Request $request)
    {
        $request = $request->all();

        $information = null;
        if (isset($request['id'])) {
            $information = Information::find($request['id']);
        }

        return view('admin/information', [
            'infoId' => $infoId,
            'information' => $information
        ]);
    }

    public function saveInformation(Request $request)
    {
        $request = $request->all();

        $information = null;

        if (isset($request['id'])) {
            /** @var Information $information */
            $information = Information::find($request['id']);
        } else {
            /** @var Information $information */
            $information = new Information();
            $information->information_guid = uniqid();
            $information->info_id = $request['infoId'];
        }

        $information->information_name = $request['informationName'];
        $information->information_content = $request['informationContent'];

        $information->save();
        $information->refresh();

        return redirect('/admin/' .$information->info_id. '/information-list?pageNumber=1&pageLimit=10');
    }

    public function deleteInformation(Request $request)
    {
        $request = $request->all();

        $information = Information::find($request['id']);
        $information->delete();
        return redirect()->back();
    }
}
