<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Info;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function showGalleryList(Request $request)
    {
        $request = $request->all();

        $totalItemsCount = Gallery::count();

        if (!isset($request['pageLimit']) || !$request['pageNumber']) {
            return redirect('/admin/gallery-list?pageNumber=1&pageLimit=10');
        }

        $pageLimit = intval($request['pageLimit']);
        $pageNumber = intval($request['pageNumber']);

        $galleryList = Gallery::orderBy('id', 'desc')
            ->limit($pageLimit)
            ->offset($pageLimit * ($pageNumber - 1))
            ->get();

        return view('admin/gallery-list', [
            'galleryList' => $galleryList,
            'pageNumber' => $pageNumber,
            'pageLimit' => $pageLimit,
            'pageTotal' => ceil($totalItemsCount / $pageLimit)
        ]);

    }

    public function showGallery(Request $request)
    {
        $request = $request->all();

        $gallery = null;
        if (isset($request['id'])) {
            $gallery = Gallery::find($request['id']);
        }
        return view('admin/gallery' , ['gallery' => $gallery]);
    }

    public function saveGallery(Request $request)
    {
        $request = $request->all();

        $gallery = null;
        if (isset($request['id'])) {
            /** @var Gallery $gallery */
            $gallery = Gallery::find($request['id']);
        }else {
            /** @var Gallery $gallery */
            $gallery = new Gallery();
            $gallery->gallery_guid = uniqid();
        }
        $gallery->gallery_name = $request['name'];

        $gallery->save();

        return redirect('/admin/gallery-list?pageNumber=1&pageLimit=10');
    }

    public function deleteGallery(Request $request)
    {
        $request = $request->all();

        $gallery = Gallery::find($request['id']);
        $gallery->delete();
        return redirect()->back();
    }
}
