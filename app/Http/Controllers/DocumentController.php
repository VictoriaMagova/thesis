<?php

namespace App\Http\Controllers;


use App\Helpers\DocumentTrait;

class DocumentController extends Controller
{
    use DocumentTrait;
}
