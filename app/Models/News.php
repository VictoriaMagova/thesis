<?php

namespace App\Models;

/**
 * Class News
 * @package App\Models
 *
 * @property int $id
 * @property string $news_guid
 * @property string $title
 * @property string $news_content
 * @property string $created_at
 *
 */
class News extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}