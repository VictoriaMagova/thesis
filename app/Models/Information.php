<?php

namespace App\Models;
/**
* Class Information
 * @package App\Models
*
 * @property int $id
* @property string $information_guid
 * @property int $info_id
* @property string $information_name
 * @property string $information_content
*/

class Information extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'information';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
