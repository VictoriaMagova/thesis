<?php

namespace App\Models;

/**
 * Class Info
 * @package App\Models
 *
 * @property int $id
 * @property string $info_guid
 * @property string $info_name
 */
class Info extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'info';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
