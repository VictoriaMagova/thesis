<?php

namespace App\Models;
/**
 * Class Image
 * @package App\Models
 *
 * @property int $id
 * @property string $gallery_id
 * @property int $image_guid
 * @property string $image_url
 */

class Image extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'image';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
