<?php
namespace App\Models;
/**
 * Class Document
 * @package App\Models
 *
 * @property int $id
 * @property string $document_guid
 * @property string $document_url
 * @property string $document_name
 *
 */

class Document extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'document';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
