<?php

namespace App\Models;
/**
* Class Gallery
 * @package App\Models
*
 * @property int $id
* @property string $gallery_guid
* @property string $gallery_name
*/

class Gallery extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gallery';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
