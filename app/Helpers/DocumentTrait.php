<?php

namespace App\Helpers;

use App\Models\Document;

trait DocumentTrait
{
    public function getDocument($id)
    {
        if(!$id || $id == ''){
            abort(404);
        }

        $document = Document::find($id);

        if(!$document) {
            abort(404);
        }

        $file = public_path() . $document->document_url;

        return response()->file($file);
    }

}