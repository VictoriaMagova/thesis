<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateDefaultTables extends Migration
{
    public function up()
    {
        DB::beginTransaction();

        $sql = "CREATE TABLE news (
                  id      bigint(10)  NOT NULL AUTO_INCREMENT,
                  news_guid    varchar(36) NOT NULL,
                  title        varchar(45) DEFAULT NULL,
                  news_content text        DEFAULT NULL,
                  created_at   timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
                
                  PRIMARY KEY (id)
                );";

        DB::statement($sql);

        $sql = "CREATE TABLE document (
                  id   bigint(10)  NOT NULL AUTO_INCREMENT,
                  document_guid varchar(36) NOT NULL,
                  document_url  varchar(100)DEFAULT NULL,
                  document_name varchar(45) DEFAULT NULL,
                
                  PRIMARY KEY (id)
                )";

        DB::statement($sql);

        $sql = "CREATE TABLE info (
                  id      bigint(10)  NOT NULL AUTO_INCREMENT,
                  info_guid    varchar(36) NOT NULL,
                  info_name   varchar(45) DEFAULT NULL,
                
                  PRIMARY KEY (id)
                )";

        DB::statement($sql);

        $sql = "CREATE TABLE information (
                  id           bigint(10)  NOT NULL AUTO_INCREMENT,
                  information_guid         varchar(36) NOT NULL,
                  info_id                  bigint(10)  NOT NULL,
                  information_name         varchar(36) DEFAULT NULL,
                  logo_image_guid          varchar(36) DEFAULT NULL,
                  information_content      text        DEFAULT NULL,
                  information_gallery_guid varchar(36) DEFAULT NULL,
                
                  PRIMARY KEY (id),
                  CONSTRAINT fk_info_id FOREIGN KEY (info_id) REFERENCES info (id)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION
                )";

        DB::statement($sql);

        $sql = "CREATE TABLE gallery (
                  id      bigint(10)  NOT NULL AUTO_INCREMENT,
                  gallery_guid    varchar(36) NOT NULL,
                  gallery_name    varchar(36) NOT NULL,
                
                  PRIMARY KEY (id)
                )";

        DB::statement($sql);

        $sql = "CREATE TABLE image (
                  id   bigint(10)  NOT NULL AUTO_INCREMENT,
                  gallery_id bigint(10)  NOT NULL,
                  image_guid varchar(36) NOT NULL,
                  image_url varchar(50)  DEFAULT NULL,
                
                  PRIMARY KEY (id),
                  CONSTRAINT fk_gallery_id FOREIGN KEY (gallery_id) REFERENCES gallery (id)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION
                )";

        DB::statement($sql);

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_tables');
    }
}
