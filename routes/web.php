<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', function () {
    return redirect('/hirek');
});

Route::prefix('admin')->middleware('auth')->group(function () {
    //Logout
    Route::get('/logout', 'HomeController@logout');

    //News
    Route::get('/news-list', 'Admin\NewsController@showNewsList');
    Route::get('/news', 'Admin\NewsController@showNews');
    Route::post('/save-news', 'Admin\NewsController@saveNews');
    Route::get('/delete-news', 'Admin\NewsController@deleteNews');

    //Document
    Route::get('/document-list', 'Admin\DocumentController@showDocumentList');
    Route::get('/document', 'Admin\DocumentController@showDocument');
    Route::post('/save-document', 'Admin\DocumentController@saveDocument');
    Route::get('/delete-document', 'Admin\DocumentController@deleteDocument');
    Route::get('/document/{id}', 'Admin\DocumentController@getDocument');

    //Information
    Route::get('/{infoId}/information-list', 'Admin\InformationController@showInformationList');
    Route::get('/{infoId}/information', 'Admin\InformationController@showInformation');
    Route::post('/save-information', 'Admin\InformationController@saveInformation');
    Route::get('/delete-information', 'Admin\InformationController@deleteInformation');

    //Info
    Route::get('/info-list', 'Admin\InfoController@showInfoList');
    Route::get('/info', 'Admin\InfoController@showInfo');
    Route::post('/save-info', 'Admin\InfoController@saveInfo');
    Route::get('/delete-info', 'Admin\InfoController@deleteInfo');

    //Gallery
    Route::get('/gallery-list', 'Admin\GalleryController@showGalleryList');
    Route::get('/gallery', 'Admin\GalleryController@showGallery');
    Route::post('/save-gallery', 'Admin\GalleryController@saveGallery');
    Route::get('/delete-gallery', 'Admin\GalleryController@deleteGallery');

    //Image
    Route::get('/{galleryId}/image-list', 'Admin\ImageController@showImageList');
    Route::get('/{galleryId}/image', 'Admin\ImageController@showImage');
    Route::post('/save-image', 'Admin\ImageController@saveImage');
    Route::get('/delete-image', 'Admin\ImageController@deleteImage');
});


//Site oldal
Route::get('/hirek', 'NewsController@showNewsList');

//Document
Route::get('/dokumentumok', 'DocumentController@showDocumentList');
Route::get('/document/{id}', 'DocumentController@getDocument');

//Information
Route::get('/informacio', 'InformationController@showInformation');

//Info
Route::get('/info', 'InfoController@showInfoList');

//Gallery
Route::get('/galleriak', 'GalleryController@showGalleryList');
Route::get('/galleria/{id}', 'GalleryController@showGallery');

Route::get('/home', 'HomeController@index')->name('home');
